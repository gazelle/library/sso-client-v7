package net.ihe.gazelle.ssov7.configuration.interlay;

import net.ihe.gazelle.ssov7.configuration.domain.ApplicationConfigurationService;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Name("applicationConfigurationBean")
@Scope(ScopeType.EVENT)
@AutoCreate
public class ApplicationConfigurationBean {

    @In(value = "applicationConfigurationService")
    private ApplicationConfigurationService applicationConfigurationService;

    public boolean isUserRegistrationEnabled() {
        return applicationConfigurationService.isUserRegistrationEnabled();
    }

    public String getUserRegistrationUrl() {
        return applicationConfigurationService.getUserRegistrationUrl();
    }

    public String getUserAccountUrl() {
        return applicationConfigurationService.getUserAccountUrl();
    }

    public String getUsersManagementUrl() {
        return applicationConfigurationService.getUsersManagementUrl();
    }

    public boolean showNewLoginMenu(){
        return applicationConfigurationService.shouldShowNewMenu();
    }

}
