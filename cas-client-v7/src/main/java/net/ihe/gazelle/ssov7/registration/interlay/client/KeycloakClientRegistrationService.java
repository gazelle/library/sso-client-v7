package net.ihe.gazelle.ssov7.registration.interlay.client;

import net.ihe.gazelle.ssov7.authn.interlay.dto.KeycloakClientDTO;
import net.ihe.gazelle.ssov7.registration.domain.SSOClientRegistrationException;
import net.ihe.gazelle.ssov7.registration.domain.SSOClientRegistrationService;
import net.ihe.gazelle.ssov7.authn.interlay.client.ServerErrorRetryHandler;
import net.ihe.gazelle.ssov7.authn.interlay.client.ServerErrorRetryStrategy;
import net.ihe.gazelle.ssov7.registration.interlay.dto.CasKeycloakClientDTO;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpStatus;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.*;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;


public class KeycloakClientRegistrationService implements SSOClientRegistrationService {
    public static final String REALM = "gazelle";
    public static final String CLIENTS_URL = "/admin/realms/" + REALM + "/clients";
    private static final int RETRY_COUNT = 3;
    public static final String BEARER = "Bearer ";
    public static final String REALMS_MASTER_PROTOCOL_OPENID_CONNECT_TOKEN = "/realms/master/protocol/openid-connect/token";
    private String accessToken;
    private final String keycloakUrl;
    private final Integer retryInterval;
    private static final Logger LOG = LoggerFactory.getLogger(KeycloakClientRegistrationService.class);

    public KeycloakClientRegistrationService(String keycloakUrl, Integer retryInterval) {
        accessToken = null;
        this.keycloakUrl = keycloakUrl;
        this.retryInterval = retryInterval;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void registerClient(String clientId, String applicationUrl) {
        LOG.trace("registerClient");
        if (clientId == null || applicationUrl == null)
            throw new IllegalArgumentException(clientId == null ? "clientId" : "applicationUrl" + " is null");

        KeycloakClientDTO clientRepresentation = createCasKeycloakClientRepresentation(clientId, applicationUrl);

        String clientRepresentationJson = getClientRepresentationJson(clientRepresentation);

        registerClientRequest(clientId, clientRepresentationJson);
        LOG.trace("registerClient successful");
    }

    private void registerClientRequest(String clientId, String clientRepresentationJson) {
        LOG.trace("registerClientRequest with clientId {}", clientId);
        try (CloseableHttpClient httpClient = HttpClients.custom()
                .setServiceUnavailableRetryStrategy(new ServerErrorRetryStrategy(retryInterval, RETRY_COUNT))
                .setRetryHandler(new ServerErrorRetryHandler(RETRY_COUNT))
                .build()) {

            String id = getIdByClientId(clientId);
            HttpEntityEnclosingRequestBase registerClientRequest =
                    (id == null) ?
                            new HttpPost(keycloakUrl + CLIENTS_URL) :
                            new HttpPut(keycloakUrl + CLIENTS_URL + "/" + id);

            registerClientRequest.addHeader(HttpHeaders.AUTHORIZATION, BEARER + getAccessToken());
            registerClientRequest.addHeader(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_JSON.getMimeType());
            registerClientRequest.setEntity(new StringEntity(clientRepresentationJson));

            try (CloseableHttpResponse response = httpClient.execute(registerClientRequest)) {
                assertNoErrorStatus(response);
                EntityUtils.consume(response.getEntity());
            }

        } catch (IOException e) {
            throw new SSOClientRegistrationException(String.format("registerClientRequest failed with clientId %s", clientId), e);
        }
        LOG.trace("registerClientRequest successful");
    }

    private String getClientRepresentationJson(KeycloakClientDTO clientRepresentation) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(clientRepresentation);
        } catch (IOException e) {
            throw new SSOClientRegistrationException(e);
        }
    }

    private void assertNoErrorStatus(CloseableHttpResponse response) {
        if (response.getStatusLine().getStatusCode() >= HttpStatus.SC_BAD_REQUEST) {
            String message = response.getStatusLine().getStatusCode() + " " +
                    response.getStatusLine().getReasonPhrase();
            try {
                String body = EntityUtils.toString(response.getEntity());
                if (body != null)
                    throw new SSOClientRegistrationException(message + " with body: " + body);
                throw new SSOClientRegistrationException(message);
            } catch (IOException e) {
                throw new SSOClientRegistrationException(message);
            }
        }
    }

    /**
     * Get the access token from keycloak
     * It has an expiration time of 1 minute by default
     *
     * @return the keycloak access token
     */
    String getAccessToken() {
        LOG.trace("enter getAccessToken");
        if (accessToken == null) {
            try (CloseableHttpClient httpClient = buildCloseableHttpClient()) {
                HttpPost accessTokenRequest = new HttpPost(keycloakUrl + REALMS_MASTER_PROTOCOL_OPENID_CONNECT_TOKEN);
                accessTokenRequest.addHeader(HttpHeaders.CONTENT_TYPE, "application/x-www-form-urlencoded");
                accessTokenRequest.setEntity(new UrlEncodedFormEntity(Arrays.asList(
                        new BasicNameValuePair("username", System.getenv("GZL_SSO_ADMIN_USER")),
                        new BasicNameValuePair("password", System.getenv("GZL_SSO_ADMIN_PASSWORD")),
                        new BasicNameValuePair("grant_type", "password"),
                        new BasicNameValuePair("client_id", "admin-cli")
                )));
                try (CloseableHttpResponse response = httpClient.execute(accessTokenRequest)) {
                    assertNoErrorStatus(response);
                    HttpEntity responseEntity = response.getEntity();
                    this.accessToken = getAccessTokenFromJson(responseEntity.getContent());
                    EntityUtils.consume(responseEntity);
                }
            } catch (IOException e) {
                throw new SSOClientRegistrationException("getAccessToken failed", e);
            }
        }
        LOG.trace("getAccessToken successful");
        return accessToken;
    }

    String getIdByClientId(String clientId) {
        LOG.trace("getIdByClientId with clientId:{}", clientId);
        try (CloseableHttpClient httpClient = buildCloseableHttpClient()) {

            HttpGet clientsRequest = new HttpGet(keycloakUrl + "/admin/realms/" + REALM + "/clients");
            clientsRequest.addHeader(HttpHeaders.AUTHORIZATION, BEARER + getAccessToken());

            try (CloseableHttpResponse response = httpClient.execute(clientsRequest)) {
                assertNoErrorStatus(response);
                HttpEntity responseEntity = response.getEntity();
                String id = getIdByClientIdInJson(clientId, responseEntity.getContent());
                EntityUtils.consume(responseEntity);
                LOG.trace("getIdByClientId successful");
                return id;
            }
        } catch (IOException e) {
            throw new SSOClientRegistrationException(String.format("getIdByClientId failed with clientId %s", clientId), e);
        }
    }

    private CloseableHttpClient buildCloseableHttpClient() {
        return HttpClients.custom()
                .setServiceUnavailableRetryStrategy(new ServerErrorRetryStrategy(retryInterval, RETRY_COUNT))
                .setRetryHandler(new ServerErrorRetryHandler(RETRY_COUNT))
                .build();
    }

    private String getIdByClientIdInJson(String clientId, InputStream response) throws IOException {
        try {
            JsonNode jsonNode = new ObjectMapper().readTree(response);
            if (jsonNode.isArray()) {
                Iterator<JsonNode> jsonNodeIterator = jsonNode.getElements();
                while (jsonNodeIterator.hasNext()) {
                    JsonNode currentNode = jsonNodeIterator.next();
                    if (currentNode.get("clientId").getTextValue().equals(clientId))
                        return currentNode.get("id").getTextValue();
                }
            }
            return null;
        } finally {
            response.close();
        }
    }

    private String getAccessTokenFromJson(InputStream response) throws IOException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode jsonNode = mapper.readTree(response);
            return jsonNode.get("access_token").getTextValue();
        } finally {
            response.close();
        }
    }

    private KeycloakClientDTO createCasKeycloakClientRepresentation(String clientId, String applicationUrl) {
        //checking and removing ending '/'
        if (applicationUrl.charAt(applicationUrl.length() - 1) == '/')
            applicationUrl = applicationUrl.substring(0, applicationUrl.length() - 1);

        KeycloakClientDTO clientRepresentation = new CasKeycloakClientDTO();
        clientRepresentation.setClientId(clientId);
        clientRepresentation.setName(clientId);
        clientRepresentation.setBaseUrl(applicationUrl);
        clientRepresentation.setRedirectUris(Collections.singletonList(applicationUrl + "/*"));
        clientRepresentation.setClientAuthenticatorType("client-secret");
        clientRepresentation.setDefaultClientScopes(Collections.singletonList("cas-client-scope"));
        return clientRepresentation;
    }
}
