package net.ihe.gazelle.ssov7.gum.client.application.service;

import net.ihe.gazelle.ssov7.gum.client.application.User;
import net.ihe.gazelle.ssov7.gum.client.interlay.client.utils.UserSearchParams;
import net.ihe.gazelle.ssov7.gum.client.interlay.model.entity.UserSearchResult;

import java.util.List;
import java.util.Map;

/**
 * @author Clément LAGORCE, Claude LUSSEAU
 */
public interface UserService {

   /**
    * Search for users according to criteria. Result is returned with pagination.
    *
    * @param userSearchParams : the search parameters to filter users
    *
    * @return the search result that contains the selected offset, the limit, the total count of matches and the List of
    * all users corresponding to the defined filter parameters for the selected page (offset and limit).
    */
   UserSearchResult searchAndFilter(UserSearchParams userSearchParams);

   /**
    * Same as searchAndFilter but without pagination. WARNING this can take a lot of time and resources if
    * filtering criteria are not restrictive enough.
    *
    * @param userSearchParams : the search parameters to filter users
    *
    * @return List of all users corresponding to the defined filter parameters
    */
   List<User> searchNoLimit(UserSearchParams userSearchParams);

   /**
    * Get distinct possibles values of a property, along with the count of all filtered users that have these values.
    *
    * @param propertyName   : the name of the user's property to list and count values for (firstName, lastName,
    *                       organizationId, roles, activated...).
    * @param search         : filter like for firstName, lastName or email
    * @param organizationId : filter by organizationId
    * @param role           : filter by Role
    * @param activated      : filter by activation status
    *
    * @return A String-Long map containing as key the existing distincts values for the given property and as value the
    * count of all filtered users having this value.
    */
   Map<String, Long> getValueCount(String propertyName, String search, String organizationId, String role, Boolean activated);

   /**
    * Target endpoint : GUM UserController getUserById method /gum/rest/users/{userId}
    *
    * @param userId : id of the user to be edited
    *
    * @return The user found with the given id returned as a GumUser.
    *
    * @throws java.util.NoSuchElementException : if the user is not found.
    * @throws IllegalArgumentException         : if the userId is null
    */
   User getUserById(String userId);


   /**
    * @param userId : remove the user from cache.
    */
   void removeUserCache(String userId);

   /**
    * Return the display name of the user or the user id if the display name is not found.
    *
    * @param userId : id of the user
    *
    * @return the display name of the user
    */
   String getUserDisplayNameWithoutException(String userId);
}
