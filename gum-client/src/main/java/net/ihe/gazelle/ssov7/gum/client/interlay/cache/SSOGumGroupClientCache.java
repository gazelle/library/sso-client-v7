package net.ihe.gazelle.ssov7.gum.client.interlay.cache;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import net.ihe.gazelle.ssov7.gum.client.application.Group;
import net.ihe.gazelle.ssov7.gum.client.application.service.GroupService;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.intercept.BypassInterceptors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project sso-client-v7
 * @date 10/10/2023
 */
@AutoCreate
@Scope(ScopeType.APPLICATION)
@Name("groupServiceCache")
@BypassInterceptors
public class SSOGumGroupClientCache implements GroupService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SSOGumGroupClientCache.class);
    private final GroupService groupService;

    private final Cache<String, Map<String, Group>> listRolesCached = CacheBuilder.newBuilder().maximumSize(20).expireAfterWrite(10, TimeUnit.MINUTES).expireAfterAccess(10, TimeUnit.MINUTES).build();

    public SSOGumGroupClientCache(GroupService groupService) {
        this.groupService = groupService;
    }

    public SSOGumGroupClientCache() {
        this.groupService = (GroupService) Component.getInstance("ssoGroupClient", true);
    }

    @Override
    public List<Group> getGroups() {
        LOGGER.trace("getGroups from cache");
        Map<String, Group> groupMap = listRolesCached.getIfPresent("allRoles");
        List<Group> groupList = new ArrayList<>();
        if (groupMap != null) {
            LOGGER.debug("getGroups Cached => {}", groupMap.size());
        } else {
            groupMap = new HashMap<>();
            List<Group> groups = groupService.getGroups();
            LOGGER.debug("getGroups is null: call service => {}", groups.size());
            for (Group group : groups) {
                groupMap.put(group.getId(), group);
            }
            listRolesCached.put("allRoles", groupMap);
        }
        for (Map.Entry<String, Group> entry : groupMap.entrySet()) {
            groupList.add(entry.getValue());
        }
        return groupList;
    }
}
