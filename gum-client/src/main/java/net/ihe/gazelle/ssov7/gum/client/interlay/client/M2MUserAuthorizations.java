package net.ihe.gazelle.ssov7.gum.client.interlay.client;


import net.ihe.gazelle.ssov7.authn.interlay.adapter.GazelleIdentityImpl;
import net.ihe.gazelle.ssov7.gum.client.application.Role;
import net.ihe.gazelle.ssov7.m2m.server.domain.M2MAuthorization;
import net.ihe.gazelle.ssov7.m2m.server.interlay.M2MPrincipal;

public class M2MUserAuthorizations {


    public static final M2MAuthorization ANY_USERS_CREATE = new M2MAuthorization() {
        @Override
        public boolean isGranted(Object... scope) {
            GazelleIdentityImpl instance = GazelleIdentityImpl.instance();
            return instance.isLoggedIn()
                    && instance.hasRole(Role.ADMIN)
                    && ((M2MPrincipal) instance.getPrincipal()).getScopes().contains("any-users:create");
        }
    };

    public static final M2MAuthorization ANY_USERS_READ_PRIVATE = new M2MAuthorization() {
        @Override
        public boolean isGranted(Object... scope) {
            GazelleIdentityImpl instance = GazelleIdentityImpl.instance();
            return instance.isLoggedIn()
                    && instance.hasRole(Role.ADMIN)
                    && ((M2MPrincipal) instance.getPrincipal()).getScopes().contains("any-users:read-private");
        }
    };
    public static final M2MAuthorization ANY_USERS_READ_PUBLIC = new M2MAuthorization() {
        @Override
        public boolean isGranted(Object... scope) {
            GazelleIdentityImpl instance = GazelleIdentityImpl.instance();
            return instance.isLoggedIn() && instance.hasRole(Role.ADMIN)
                    && ((M2MPrincipal) instance.getPrincipal()).getScopes().contains("any-users:read-public");
        }
    };

    public static final M2MAuthorization ANY_USERS_UPDATE = new M2MAuthorization() {
        @Override
        public boolean isGranted(Object... scope) {
            GazelleIdentityImpl instance = GazelleIdentityImpl.instance();
            return instance.isLoggedIn()
                    && instance.hasRole(Role.ADMIN)
                    && ((M2MPrincipal) instance.getPrincipal()).getScopes().contains("any-users:update");
        }
    };
    public static final M2MAuthorization MY_ACCOUNT_READ = new M2MAuthorization() {
        @Override
        public boolean isGranted(Object... scope) {
            GazelleIdentityImpl instance = GazelleIdentityImpl.instance();
            return instance.isLoggedIn()
                    && instance.hasRole(Role.ADMIN)
                    && ((M2MPrincipal) instance.getPrincipal()).getScopes().contains("my-account:read");
        }
    };
    public static final M2MAuthorization MY_ACCOUNT_UPDATE = new M2MAuthorization() {
        @Override
        public boolean isGranted(Object... scope) {
            GazelleIdentityImpl instance = GazelleIdentityImpl.instance();
            return instance.isLoggedIn()
                    && instance.hasRole(Role.ADMIN)
                    && ((M2MPrincipal) instance.getPrincipal()).getScopes().contains("my-account:update");
        }
    };

    public static final M2MAuthorization MY_ORGA_USERS_CREATE = new M2MAuthorization() {
        @Override
        public boolean isGranted(Object... scope) {
            GazelleIdentityImpl instance = GazelleIdentityImpl.instance();
            return instance.isLoggedIn()
                    && instance.hasRole(Role.ADMIN)
                    && ((M2MPrincipal) instance.getPrincipal()).getScopes().contains("my-orga-users:create");
        }
    };
    public static final M2MAuthorization MY_ORGA_USERS_READ_PRIVATE = new M2MAuthorization() {
        @Override
        public boolean isGranted(Object... scope) {
            GazelleIdentityImpl instance = GazelleIdentityImpl.instance();
            return instance.isLoggedIn()
                    && instance.hasRole(Role.ADMIN)
                    && ((M2MPrincipal) instance.getPrincipal()).getScopes().contains("my-orga-users:read-private");
        }
    };
    public static final M2MAuthorization MY_ORGA_USERS_READ_PUBLIC = new M2MAuthorization() {
        @Override
        public boolean isGranted(Object... scope) {
            GazelleIdentityImpl instance = GazelleIdentityImpl.instance();
            return instance.isLoggedIn() && instance.hasRole(Role.ADMIN)
                    && ((M2MPrincipal) instance.getPrincipal()).getScopes().contains("my-orga-users:read-public");
        }
    };
    public static final M2MAuthorization MY_ORGA_USERS_UPDATE = new M2MAuthorization() {
        @Override
        public boolean isGranted(Object... scope) {
            GazelleIdentityImpl instance = GazelleIdentityImpl.instance();
            return instance.isLoggedIn() && instance.hasRole(Role.ADMIN)
                    && ((M2MPrincipal) instance.getPrincipal()).getScopes().contains("my-orga-users:update");
        }
    };


    private M2MUserAuthorizations() {
        // Hide constructor, this class is exclusively composed of static members.
    }
}
