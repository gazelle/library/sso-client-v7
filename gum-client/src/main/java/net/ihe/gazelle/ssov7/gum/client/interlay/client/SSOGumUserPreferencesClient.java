package net.ihe.gazelle.ssov7.gum.client.interlay.client;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.ihe.gazelle.ssov7.gum.client.application.UserPreference;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserPreferencesService;
import net.ihe.gazelle.ssov7.gum.client.interlay.exception.GumSsoClientHttpErrorException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

public class SSOGumUserPreferencesClient implements UserPreferencesService {
    private static final String PREFERENCES_PATH = "preferences";
    private static final String CONTENT_TYPE_IMAGE_JPEG = "image/jpeg";
    private final DefaultGumClient gumClient;
    private final String gumUserPath;

    public SSOGumUserPreferencesClient() {
        gumClient = new DefaultGumClient();
        gumUserPath = gumClient.getGumUrl() + "/users";
    }

    @Override
    public UserPreference getUserPreferencesByUserId(String userId) {
        try (CloseableHttpClient httpClient = gumClient.buildCloseableHttpClient()) {
            String preferenceEndpoint = gumUserPath + "/" + userId + "/" + PREFERENCES_PATH;
            URI uri = new URIBuilder(preferenceEndpoint).build();
            HttpGet httpGet = new HttpGet(uri);
            try (CloseableHttpResponse response = httpClient.execute(httpGet)) {
                HttpEntity entity = response.getEntity();
                gumClient.assertNoErrorStatus(response.getStatusLine().getStatusCode(), response.getStatusLine().getReasonPhrase(), entity);
                return gumClient.extractJsonValue(entity, new TypeReference<UserPreference>() {
                });
            }
        } catch (IOException | URISyntaxException e) {
            throw new GumSsoClientHttpErrorException(e);
        }
    }


    @Override
    public Object getSingleUserPreference(String userId, String preferenceName) {
        try (CloseableHttpClient httpClient = gumClient.buildCloseableHttpClient()) {
            String preferenceEndpoint = gumUserPath + "/" + userId + "/" + PREFERENCES_PATH + "/" + preferenceName;
            URI uri = new URIBuilder(preferenceEndpoint).build();
            HttpGet httpGet = new HttpGet(uri);
            try (CloseableHttpResponse response = httpClient.execute(httpGet)) {
                HttpEntity entity = response.getEntity();
                gumClient.assertNoErrorStatus(response.getStatusLine().getStatusCode(), response.getStatusLine().getReasonPhrase(), entity);
                if ("languagesSpoken".equals(preferenceName)) {
                    return gumClient.extractJsonValue(entity, new TypeReference<List<String>>() {
                    });
                }
                return gumClient.extractJsonValue(entity, new TypeReference<Object>() {
                });
            }
        } catch (IOException | URISyntaxException e) {
            throw new GumSsoClientHttpErrorException(e);
        }
    }

    @Override
    public byte[] getUserProfilePicture(String userId, String format) {
        try (CloseableHttpClient httpClient = gumClient.buildCloseableHttpClient()) {
            String preferenceEndpoint = gumUserPath + "/" + userId + "/" + PREFERENCES_PATH + "/picture";
            URI uri = new URIBuilder(preferenceEndpoint).setParameter("format", format).build();
            HttpGet httpGet = new HttpGet(uri);
            httpGet.setHeader(HttpHeaders.CONTENT_TYPE, CONTENT_TYPE_IMAGE_JPEG);

            try (CloseableHttpResponse response = httpClient.execute(httpGet)) {
                HttpEntity entity = response.getEntity();
                gumClient.assertNoErrorStatus(response.getStatusLine().getStatusCode(), response.getStatusLine().getReasonPhrase(), entity);
                return EntityUtils.toByteArray(entity);
            }
        } catch (IOException | URISyntaxException e) {
            throw new GumSsoClientHttpErrorException(e);
        }
    }

    @Override
    public UserPreference updateUserPreferences(String userId, UserPreference userPreference) {
        try (CloseableHttpClient httpClient = gumClient.buildCloseableHttpClient()) {
            String preferenceEndpoint = gumUserPath + "/" + userId + "/" + PREFERENCES_PATH;
            URI uri = new URIBuilder(preferenceEndpoint).build();
            HttpPut httpPut = new HttpPut(uri);
            ObjectMapper mapper = new ObjectMapper();
            String userPreferenceJson = mapper.writeValueAsString(userPreference);
            StringEntity httpEntity = new StringEntity(userPreferenceJson, ContentType.APPLICATION_JSON);
            httpPut.setEntity(httpEntity);
            try (CloseableHttpResponse response = httpClient.execute(httpPut)) {
                HttpEntity entity = response.getEntity();
                gumClient.assertNoErrorStatus(response.getStatusLine().getStatusCode(), response.getStatusLine().getReasonPhrase(), entity);
                return gumClient.extractJsonValue(entity, new TypeReference<UserPreference>() {
                });
            }
        } catch (IOException | URISyntaxException e) {
            throw new GumSsoClientHttpErrorException(e);
        }
    }

}
