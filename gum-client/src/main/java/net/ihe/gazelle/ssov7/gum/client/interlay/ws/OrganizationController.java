package net.ihe.gazelle.ssov7.gum.client.interlay.ws;

import net.ihe.gazelle.http.PATCH;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/organizations")
public interface OrganizationController {

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    Response getOrganizationById(@PathParam("id") String id);

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    Response searchForOrganizations(@Context HttpServletRequest servletRequest,
                                    @QueryParam("organizationName") String organizationName,
                                    @QueryParam("externalId") String externalId,
                                    @QueryParam("idpId") String idpId,
                                    @QueryParam("delegated") Boolean delegated);

    @GET
    @Produces(MediaType.APPLICATION_XML)
    Response getXMLOrganizations(@Context HttpServletRequest servletRequest);

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    Response createOrganization(OrganizationResource organisation);

    @PATCH
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    Response patchOrganization(@PathParam("id") String id, OrganizationResource organizationResource);
}