package net.ihe.gazelle.ssov7.gum.client.interlay.cache;

import net.ihe.gazelle.ssov7.gum.client.application.UserPreference;
import net.ihe.gazelle.ssov7.gum.client.interlay.client.SSOGumUserPreferencesClient;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class UserPreferencesClientCacheTest {

    UserPreferencesClientCache userPreferencesClientCache;
    private SSOGumUserPreferencesClient userClient;
    UserPreference userPreference;
    String userId = "adfdf-58ds4fd5s-dfdsf89";
    byte[] profilePicture = "thisIsAUserProfilePicture".getBytes(StandardCharsets.UTF_8);
    byte[] profileThumbnail = "thisIsAUserProfileThumbnail".getBytes(StandardCharsets.UTF_8);


    @Before
    public void setUp() {
        userPreference = new UserPreference();
        userPreference.setUserId(userId);
        userPreference.setLanguagesSpoken(Arrays.asList("en", "fr"));
        userPreference.setTableLabel("tableLabel");
        userPreference.setNotifiedByEmail(true);
        userPreference.setProfilePictureUri("/path/to/profile");
        userPreference.setProfileThumbnailUri("/path/to/thumbnail");

        userClient = mock(SSOGumUserPreferencesClient.class);
        userPreferencesClientCache = new UserPreferencesClientCache(userClient);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getUserPreferencesByUserId() {
        when(userClient.getUserPreferencesByUserId(anyString())).thenReturn(userPreference);
        //first call to cache, it should call service and cache response
        assertNotNull(userPreferencesClientCache.getUserPreferencesByUserId(userId));

        //Data is already cached and should not call service again
        verify(userClient, times(1)).getUserPreferencesByUserId(userId);
        assertEquals(userPreference, userPreferencesClientCache.getUserPreferencesByUserId(userId));
    }

    @Test
    public void getSingleUserPreference() {
        when(userClient.getSingleUserPreference(userId, "languagesSpoken")).thenReturn(userPreference.getLanguagesSpoken());

        assertNotNull(userPreferencesClientCache.getSingleUserPreference(userId, "languagesSpoken"));
        verify(userClient, times(1)).getSingleUserPreference(userId, "languagesSpoken");
        assertEquals(userPreference.getLanguagesSpoken(), userPreferencesClientCache.getSingleUserPreference(userId, "languagesSpoken"));
    }

    @Test
    public void getUserProfilePicture() {
        when(userClient.getUserProfilePicture(userId, "normal")).thenReturn(profilePicture);

        //first call to cache, it should call service and cache both normal picture and thumbnail
        assertNotNull(userPreferencesClientCache.getUserProfilePicture(userId, "normal"));

        //Data is already cached and should not call service again
        verify(userClient, times(1)).getUserProfilePicture(userId, "normal");

        assertArrayEquals(profilePicture, userPreferencesClientCache.getUserProfilePicture(userId, "normal"));
    }

    @Test
    public void getUserProfileThumbnail() {
        when(userClient.getUserProfilePicture(userId, "thumbnail")).thenReturn(profileThumbnail);

        //first call to cache, it should call service and cache thumbnail
        assertNotNull(userPreferencesClientCache.getUserProfilePicture(userId, "thumbnail"));

        //Data is already cached and should not call service again
        verify(userClient, times(1)).getUserProfilePicture(userId, "thumbnail");

        assertArrayEquals(profileThumbnail, userPreferencesClientCache.getUserProfilePicture(userId, "thumbnail"));
    }

    @Test
    public void updateUserPreferences() {
        UserPreference userPreferenceUpdate = new UserPreference(userPreference);
        userPreferenceUpdate.setTableLabel("roundTable");
        userPreferenceUpdate.setNotifiedByEmail(false);

        when(userClient.getUserPreferencesByUserId(anyString())).thenReturn(userPreference);
        //first call to cache, it should call service and cache response
        assertNotNull(userPreferencesClientCache.getUserPreferencesByUserId(userId));

        when(userClient.updateUserPreferences(userId, userPreferenceUpdate)).thenReturn(userPreferenceUpdate);
        userPreferencesClientCache.updateUserPreferences(userId, userPreferenceUpdate);
        assertNotNull(userPreferencesClientCache.getUserPreferencesByUserId(userId));
        assertEquals(userPreferenceUpdate, userPreferencesClientCache.getUserPreferencesByUserId(userId));
        verify(userClient, times(1)).getUserPreferencesByUserId(userId);

    }

    @Test(expected = IllegalArgumentException.class)
    public void getUserByIdNull() {
        userPreferencesClientCache.getUserPreferencesByUserId(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getUserProfilePictureNullFirstParameter() {
        userPreferencesClientCache.getUserProfilePicture(null, "normal");
    }

    @Test(expected = IllegalArgumentException.class)
    public void getUserProfilePictureNullSecondParameter() {
        userPreferencesClientCache.getUserProfilePicture(userId, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getSingleUserPreferenceNullFirstParameter() {
        userPreferencesClientCache.getSingleUserPreference(userId, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getSingleUserPreferenceNullSecondParameter() {
        userPreferencesClientCache.getSingleUserPreference(null, "pref");
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateUserPreferenceNullFirstParameter() {
        userPreferencesClientCache.updateUserPreferences(null, userPreference);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateUserPreferenceNullSecondParameter() {
        userPreferencesClientCache.updateUserPreferences(userId, null);
    }

}