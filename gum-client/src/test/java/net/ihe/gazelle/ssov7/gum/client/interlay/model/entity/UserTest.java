package net.ihe.gazelle.ssov7.gum.client.interlay.model.entity;

import net.ihe.gazelle.ssov7.gum.client.application.Group;
import net.ihe.gazelle.ssov7.gum.client.application.User;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.Test;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

/**
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project sso-client-v7
 * @date 11/09/2023
 */
public class UserTest {

    @Test
    public void testUserConstruct1OK() {
        Set<String> roles = new HashSet<>();
        roles.add("unknown_admin");
        User user = new User("John", "Doe", "john.doe@lost.com", roles, "organizationId",false, null, null, new Timestamp(1601906470000L), 0, new Timestamp(1601906470000L));
        assertEquals("John", user.getFirstName());
        assertEquals("Doe", user.getLastName());
        assertEquals("john.doe@lost.com", user.getEmail());
        assertEquals(roles, user.getGroupIds());
        assertEquals("organizationId", user.getOrganizationId());
        assertFalse(user.getActivated());
        assertEquals(1601906470000L, user.getLastLoginTimestamp().getTime());
        assertEquals(0, user.getLoginCounter().intValue());
        assertEquals(1601906470000L, user.getLastUpdateTimestamp().getTime());
    }

    @Test
    public void testUserConstruct2OK() {
        Set<String> roles = new HashSet<>();
        roles.add("unknown_admin");
        User user = new User("user1","John", "Doe", "john.doe@lost.com", roles, "organizationId", false, null, null, new Timestamp(1601906470000L), 0, new Timestamp(1601906470000L));
        assertEquals("user1", user.getId());
        assertEquals("John", user.getFirstName());
        assertEquals("Doe", user.getLastName());
        assertEquals("john.doe@lost.com", user.getEmail());
        assertEquals(roles, user.getGroupIds());
        assertEquals("organizationId", user.getOrganizationId());
        assertFalse(user.getActivated());
        assertEquals(1601906470000L, user.getLastLoginTimestamp().getTime());
        assertEquals("John Doe", user.getFirstNameAndLastName());
    }

    @Test
    public void testUserCopyConstructor() {
        Set<String> roles = new HashSet<>();
        roles.add("unknown_admin");
        User user = new User("user2","Alex", "Presso", "alex.presso@coffee.com", roles, "myOrgaId", false, null, null, new Timestamp(1601906470000L), 0, new Timestamp(1601906470000L));
        User copy = new User(user);

        assertEquals(user,copy);
    }

    @Test
    public void testUserHasRoleReturnFalseCauseNullRole() {
        Set<String> groups = new HashSet<>();
        User user = new User("user1","John", "Doe", "john.doe@lost.com", groups, "organizationId", false, null, null, new Timestamp(1601906470000L), 0, new Timestamp(1601906470000L));
        assertFalse(user.hasRole(null));
    }

    @Test
    public void testUserToString() {
        Set<String> groups = new HashSet<>();
        groups.add("unknown_admin");
        User user = new User("user2","John", "Doe", "john.doe@lost.com", groups, "organizationId", false, null, null,  null, 0, null);
        assertEquals("User{id='user2', firstName='John', lastName='Doe', email='john.doe@lost.com', groupIds=[unknown_admin], organizationId='organizationId', activated=false, lastLoginTimestamp=null, loginCounter=0, lastUpdateTimestamp=null, externalId=null, idpId=null}", user.toString());
    }

    @Test
    public void testUserHasGroup() {
        Set<String> groups = Collections.singleton(Group.GRP_GAZELLE_ADMIN);
        User user = new User("user2","John", "Doe", "john.doe@lost.com", groups, "organizationId", false, null, null,  new Timestamp(1601906470000L), 0, new Timestamp(1601906470000L));
        user.addGroupId(Group.GRP_MONITOR);
        assertTrue(user.hasGroup(Group.GRP_GAZELLE_ADMIN));
        assertTrue(user.hasGroup(Group.GRP_MONITOR));
        assertFalse(user.hasGroup(Group.GRP_TEST_DESIGNER));
    }

    @Test
    public void testEquals_Symmetric1() {
        Set<String> groups = new HashSet<>(Arrays.asList("admin_role", "user_role"));
        User x = new User("user2","John", "Doe", "john.doe@lost.com", groups, "organizationId", false, null, null,  new Timestamp(1601906470000L), 0, new Timestamp(1601906470000L));
        User y = new User("user2","John", "Doe", "john.doe@lost.com", groups, "organizationId", false, null, null,  new Timestamp(1601906470000L), 0, new Timestamp(1601906470000L));
        assertTrue(x.equals(y) && y.equals(x));
        assertEquals(x.hashCode(), y.hashCode());
    }

    @Test
    public void equalsContract() {
        EqualsVerifier.forClass(User.class).usingGetClass().suppress(Warning.NONFINAL_FIELDS).verify();
    }

    @Test
    public void testCompareTo() {
        Set<String> groups = new HashSet<>(Arrays.asList("admin_role", "user_role"));
        User x = new User("user2","John", "Doe", "john.doe@lost.com", groups, "organizationId", false, null, null,  new Timestamp(1601906470000L), 0, new Timestamp(1601906470000L));
        User y = new User("user2","John", "Doe", "john.doe@lost.com", groups, "organizationId", false, null, null,  new Timestamp(1601906470000L), 0, new Timestamp(1601906470000L));
        assertEquals(0, x.compareTo(y));
        x = new User("user2","John", "Doe", "john.doe@lost.com", groups, "organizationId", false, null, null,  new Timestamp(1601906470000L), 0, new Timestamp(1601906470000L));
        y = new User("user2","Jo", "Doe", "john.doe@lost.com", groups, "organizationId", false, null, null,  new Timestamp(1601906470000L), 0, new Timestamp(1601906470000L));
        assertEquals(2, x.compareTo(y));
        x = new User("user2",null, "Doe", "john.doe@lost.com", groups, "organizationId", false, null, null,  new Timestamp(1601906470000L), 0, new Timestamp(1601906470000L));
        y = new User("user2","Jo", "Doe", "john.doe@lost.com", groups, "organizationId", false, null, null,  new Timestamp(1601906470000L), 0, new Timestamp(1601906470000L));
        assertEquals(-1, x.compareTo(y));
        x = new User("user2","John", "Doe", "john.doe@lost.com", groups, "organizationId", false, null, null,  new Timestamp(1601906470000L), 0, new Timestamp(1601906470000L));
        y = new User("user2",null, "Doe", "john.doe@lost.com", groups, "organizationId", false, null, null,  new Timestamp(1601906470000L), 0, new Timestamp(1601906470000L));
        assertEquals(1, x.compareTo(y));
    }

    @Test
    public void testGet() {
        Set<String> groups = new HashSet<>(Arrays.asList("admin_role", "user_role"));
        User x = new User("user2","John", "Doe", "john.doe@lost.com", groups, "organizationId", false, null, null,  new Timestamp(1601906470000L), 0, new Timestamp(1601906470000L));
        User y = x.get("user2");
        assertEquals(x.getFirstName(), y.getFirstName());
        assertEquals(x.getEmail(), y.getEmail());
    }
}