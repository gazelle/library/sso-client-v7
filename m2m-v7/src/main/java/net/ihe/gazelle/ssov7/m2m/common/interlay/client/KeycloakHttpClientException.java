package net.ihe.gazelle.ssov7.m2m.common.interlay.client;

public class KeycloakHttpClientException extends RuntimeException {

    public KeycloakHttpClientException() {
        super();
    }

    public KeycloakHttpClientException(String message) {
        super(message);
    }

    public KeycloakHttpClientException(String message, Throwable cause) {
        super(message, cause);
    }

    public KeycloakHttpClientException(Throwable cause) {
        super(cause);
    }
}
