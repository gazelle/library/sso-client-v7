package net.ihe.gazelle.ssov7.m2m.server.application.jwt;


import net.ihe.gazelle.ssov7.m2m.server.domain.GazelleJwtParser;
import net.ihe.gazelle.ssov7.m2m.server.domain.GazelleJwtParserException;
import net.ihe.gazelle.ssov7.m2m.server.interlay.service.GazelleJwtParserImpl;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Objects;
import java.util.Set;

public class JWTValidator {

    String issuer = Objects.requireNonNull(System.getenv("JWT_VERIFY_ISSUER"),
            "Environment variable JWT_VERIFY_ISSUER must not be null.");

    String audience = Objects.requireNonNull(System.getenv("ROOT_TEST_BED_URL"),
            "Environment variable ROOT_TEST_BED_URL must not be null.");


    /**
     * Validate the token
     * @param token the String of the Json Web token
     * @throws JWTValidatorException if the token is not valid
     */
    public void assertValid(String token) {
        if (token == null || token.isEmpty())
            throw new IllegalArgumentException("Authorization header is not valid");

        GazelleJwtParser gazelleJwtParser;
        try {
            gazelleJwtParser = new GazelleJwtParserImpl(token);
            Set<String> tokenAudiences = gazelleJwtParser.getAudience();
            if (tokenAudiences == null || !tokenAudiences.contains(audience))
                throw new JWTValidatorException("Bad audience");

            String tokenIssuer = gazelleJwtParser.getIssuer();
            if (tokenIssuer == null || !tokenIssuer.equals(issuer))
                throw new JWTValidatorException("Bad issuer");

            Date now = new GregorianCalendar().getTime();
            Date issuedAt = gazelleJwtParser.getIssuedAt();
            if (issuedAt == null || now.before(issuedAt))
                throw new JWTValidatorException("Bad issued at time");

            if (gazelleJwtParser.getExpirationDate() == null)
                throw new JWTValidatorException("Bad expiration time");
        } catch (GazelleJwtParserException e) {
            throw new JWTValidatorException(e);
        }
    }

}
