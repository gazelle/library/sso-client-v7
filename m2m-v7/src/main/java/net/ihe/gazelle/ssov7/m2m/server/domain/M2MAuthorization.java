package net.ihe.gazelle.ssov7.m2m.server.domain;

public interface M2MAuthorization {

    boolean isGranted(Object... scope);
}
