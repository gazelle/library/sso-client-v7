package net.ihe.gazelle.ssov7.m2m.server.interlay;


import net.ihe.gazelle.ssov7.authn.interlay.Authenticator;
import net.ihe.gazelle.ssov7.authn.interlay.AuthenticatorResource;
import net.ihe.gazelle.ssov7.m2m.server.domain.GazelleJwtParser;
import net.ihe.gazelle.ssov7.m2m.server.interlay.filter.JWTValidatorFilter;
import net.ihe.gazelle.ssov7.m2m.server.interlay.service.GazelleJwtParserImpl;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.web.ServletContexts;
import org.jboss.seam.web.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashSet;

public class M2MAuthenticator implements Authenticator {
    private static final Logger LOG = LoggerFactory.getLogger(M2MAuthenticator.class);
    public static final String FAILED_TO_AUTHENTICATE_USING_M2M = "Failed to authenticate using M2M";
    public static final String NO_JWT = "no_jwt";

    @Override
    public AuthenticatorResource authenticate() {
        LOG.trace("Authenticating using M2M");
        AuthenticatorResource authenticatorResource = new AuthenticatorResource();
        String jwt = getJwt();

        if (jwt.equals(NO_JWT)) {
            LOG.trace(FAILED_TO_AUTHENTICATE_USING_M2M);
            authenticatorResource.setLoggedIn(false);
            return authenticatorResource;
        }

        GazelleJwtParser gazelleJwtParserImpl = new GazelleJwtParserImpl(jwt);
        M2MPrincipal principal = new M2MPrincipal(jwt);
        principal.setScopes(new HashSet<>(gazelleJwtParserImpl.getScope()));
        authenticatorResource.setPrincipal(principal);
        authenticatorResource.setUsername(gazelleJwtParserImpl.getAuthorizationParty());
        authenticatorResource.setEmail(System.getenv("GZL_M2M_IDENTITY_EMAIL"));
        authenticatorResource.setLastName("Gazelle");
        authenticatorResource.setFirstName(gazelleJwtParserImpl.getAuthorizationParty()
                .replace("m2m-", "")
                .replace("gazelle-", "")
                .replaceAll("\\-([a-z0-9]{3,})$", ""));

        authenticatorResource.setRoles("[" + StringUtils.join(gazelleJwtParserImpl.getRoles(), ",") + "]");
        authenticatorResource.setLoggedIn(true);

        return authenticatorResource;
    }

    @Override
    public boolean isReadyToLogin() {
        return !getJwt().equals(NO_JWT);
    }

    @Override
    public String getLogoutType() {
        return "noLogout";
    }

    @Override
    public void logout() {
        Session.instance().invalidate();
    }

    private String getJwt() {
        HttpServletRequest httpServletRequest = getM2MHttpServletRequest();
        if (httpServletRequest != null) {
            HttpSession session = httpServletRequest.getSession();
            if (session != null) {
                String sessionJwt = (String) session.getAttribute(JWTValidatorFilter.SESSION_ATTRIBUTE_JWT);
                if (sessionJwt != null) {
                    return sessionJwt;
                }
            }
        }
        return NO_JWT;
    }

    private HttpServletRequest getM2MHttpServletRequest() {
        ServletContexts contexts = ServletContexts.getInstance();
        HttpServletRequest request = null;
        if (contexts != null)
            request = contexts.getRequest();
        return request;
    }
}
