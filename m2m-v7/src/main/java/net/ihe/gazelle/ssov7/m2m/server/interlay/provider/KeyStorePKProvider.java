package net.ihe.gazelle.ssov7.m2m.server.interlay.provider;


import net.ihe.gazelle.ssov7.m2m.server.application.publickey.SSOPublicKeyProvider;
import net.ihe.gazelle.ssov7.m2m.server.application.publickey.SSOPublicKeyProviderException;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;

public class KeyStorePKProvider implements SSOPublicKeyProvider {

    private final String alias;
    private final String password;
    private final String path;

    public KeyStorePKProvider(String alias, String password, String path) {
        this.alias = alias;
        this.password = password;
        this.path = path;
    }

    @Override
    public PublicKey getPublicKey() {

        try (FileInputStream inputStream = new FileInputStream(path)) {

            KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            keyStore.load(inputStream, password.toCharArray());
            Key key = keyStore.getKey(alias, password.toCharArray());
            if (key instanceof PrivateKey) {
                // Get certificate of public key
                Certificate cert = keyStore.getCertificate(alias);
                // Get public key
                return cert.getPublicKey();
            } else {
                throw new SSOPublicKeyProviderException("Failed to get public key from keystore");
            }
        } catch (UnrecoverableKeyException | CertificateException | KeyStoreException | IOException |
                 NoSuchAlgorithmException e) {
            throw new SSOPublicKeyProviderException("Failed to get public key from keystore", e);
        }
    }
}