package net.ihe.gazelle.ssov7.m2m.server.interlay.provider;


import net.ihe.gazelle.ssov7.m2m.server.application.publickey.SSOPublicKeyProvider;
import net.ihe.gazelle.ssov7.m2m.server.application.publickey.SSOPublicKeyProviderException;

import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

import static java.nio.charset.StandardCharsets.UTF_8;

public class LocalPKProvider implements SSOPublicKeyProvider {

    private final String path;

    public LocalPKProvider(String path) {
        this.path = path;
    }

    @Override
    public PublicKey getPublicKey() {
        try {
            String publicKeyString = preparePublicKey(new String(Files.readAllBytes(Paths.get(path)), UTF_8));


            byte[] publicKeyBytes = DatatypeConverter.parseBase64Binary(publicKeyString);

            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicKeyBytes);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            return keyFactory.generatePublic(keySpec);

        } catch (IOException | NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new SSOPublicKeyProviderException("Failed to retrieve local public key", e);
        }
    }

    private String preparePublicKey(String publicKeyString) {
        //removing all useless content to get only the key
        return publicKeyString
                .replace("-----BEGIN PUBLIC KEY-----", "")
                .replace("\r", "")
                .replaceAll(System.lineSeparator(), "")
                .replace("-----END PUBLIC KEY-----", "");
    }
}
