package net.ihe.gazelle.ssov7.m2m.server.interlay.service;

import io.jsonwebtoken.*;
import io.jsonwebtoken.jackson.io.JacksonDeserializer;
import io.jsonwebtoken.lang.Maps;
import io.jsonwebtoken.security.SignatureException;
import net.ihe.gazelle.ssov7.m2m.server.domain.GazelleJwtParser;
import net.ihe.gazelle.ssov7.m2m.server.domain.GazelleJwtParserException;
import net.ihe.gazelle.ssov7.m2m.server.interlay.M2MKeyLocator;

import java.util.*;

public class GazelleJwtParserImpl implements GazelleJwtParser {

    private final Claims jwtClaims;

    public GazelleJwtParserImpl(String token) {
        jwtClaims = initParser(token);
    }

    private Claims initParser(String token) {
        if (token == null)
            throw new IllegalArgumentException("JWT must not be null");
        try {
            return Jwts.parser()
                    .keyLocator(new M2MKeyLocator())
                    .json(new JacksonDeserializer(Maps.of("roles", ArrayList.class).build()))
                    .build()
                    .parse(token)
                    .accept(Jws.CLAIMS)
                    .getPayload();
        } catch (ExpiredJwtException e) {
            throw new GazelleJwtParserException("JWT is expired");
        } catch (SignatureException e) {
            throw new GazelleJwtParserException("Signature does not match provided key");
        } catch (PrematureJwtException e) {
            throw new GazelleJwtParserException("Issued date is after current date");
        }

    }

    @Override
    public Date getIssuedAt() {
        return jwtClaims.getIssuedAt();
    }

    @Override
    public Set<String> getAudience() {
        return jwtClaims.getAudience();
    }

    @Override
    public String getIssuer() {
        return jwtClaims.getIssuer();
    }

    @Override
    public Date getExpirationDate() {
        return jwtClaims.getExpiration();
    }

    @Override
    public String getAuthorizationParty() {
        return jwtClaims.get("azp", String.class);
    }

    @Override
    public List<String> getRoles() {
        return jwtClaims.get("roles", ArrayList.class);
    }

    @Override
    public List<String> getScope() {
        return Arrays.asList(jwtClaims.get("scope", String.class).split(" "));
    }
}
