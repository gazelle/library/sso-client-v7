package net.ihe.gazelle.ssov7.m2m.server.interlay.provider;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import io.jsonwebtoken.security.Jwks;
import io.jsonwebtoken.security.PrivateJwk;
import net.ihe.gazelle.ssov7.m2m.server.application.publickey.SSOPublicKeyProviderException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Collections;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static org.junit.Assert.assertEquals;

public class KeycloakPKProviderTest {
    private final int port = Integer.parseInt(System.getProperty("testMockPort"));

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(options().port(port));
    String path = "src/test/resources/server.keystore";
    String password = "changeit";
    String alias = "providerTest";
    private X509Certificate certificate;
    private KeyPair pair;
    private PrivateJwk<PrivateKey, PublicKey, ?> jwk;

    @Before
    public void setUp() throws KeyStoreException, NoSuchAlgorithmException {
        certificate = (X509Certificate) getCertificateFromKeystore(path, alias, password);
        pair = getKeyPair();
        jwk = getPrivateKeyPublicKeyPrivateJwk();
    }

    @Test
    public void getPublicKeyTest() {
        String kid = jwk.getId();
        String jsonBody = "{\"" +
                "keys\":[" +
                "{\"kid\":\"" + kid + "\"," +
                "\"kty\":\"" + jwk.get("kty") + "\"," +
                "\"n\":\"" + jwk.get("n") + "\"," +
                "\"e\":\"" + jwk.get("e") + "\"," +
                "\"x5c\": [\"" + jwk.get("x5c").toString().replace("[", "").replace("]", "") + "\"]" +
                "}]" +
                "}";
        wireMockRule.stubFor(get("/realm/testPK")
                .willReturn(ok()
                        .withHeader("Content-type", "net/ihe/gazelle/application/json")
                        .withBody(jsonBody)
                ));

        KeycloakPKProvider ssoPublicKeyProvider = new KeycloakPKProvider("http://localhost:" + port + "/realm/testPK", kid);
        assertEquals(pair.getPublic(), ssoPublicKeyProvider.getPublicKey());
    }

    @Test(expected = SSOPublicKeyProviderException.class)
    public void getPublicKeyTestBadKeyId() {
        String kid = jwk.getId();
        String jsonBody = "{\"" +
                "keys\":[" +
                "{\"kid\":\"badId\"," +
                "\"kty\":\"" + jwk.get("kty") + "\"," +
                "\"n\":\"" + jwk.get("n") + "\"," +
                "\"e\":\"" + jwk.get("e") + "\"," +
                "\"x5c\": [\"" + jwk.get("x5c").toString().replace("[", "").replace("]", "") + "\"]" +
                "}]" +
                "}";
        wireMockRule.stubFor(get("/realm/testPK")
                .willReturn(ok()
                        .withHeader("Content-type", "net/ihe/gazelle/application/json")
                        .withBody(jsonBody)
                ));

        KeycloakPKProvider ssoPublicKeyProvider = new KeycloakPKProvider("http://localhost:" + port + "/realm/testPK", kid);
        ssoPublicKeyProvider.getPublicKey();
    }


    @Test(expected = IllegalArgumentException.class)
    public void getPublicKeyEmptyKeyId() {
        KeycloakPKProvider ssoPublicKeyProvider = new KeycloakPKProvider("http://localhost:" + port + "/realm/testPK", "");
        ssoPublicKeyProvider.getPublicKey();
    }

    @Test(expected = IllegalArgumentException.class)
    public void getPublicKeyKeyIdNull() {
        KeycloakPKProvider ssoPublicKeyProvider = new KeycloakPKProvider("http://localhost:" + port + "/realm/testPK", null);
        ssoPublicKeyProvider.getPublicKey();
    }


    private PrivateJwk<PrivateKey, PublicKey, ?> getPrivateKeyPublicKeyPrivateJwk() {
        return Jwks.builder()
                .key(pair.getPublic())
                .privateKey(pair.getPrivate())
                .id("id")
                .x509Chain(Collections.singletonList(certificate))
                .build();
    }

    private KeyPair getKeyPair() throws NoSuchAlgorithmException {
        KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
        generator.initialize(2048);
        return generator.generateKeyPair();
    }

    @Test(expected = SSOPublicKeyProviderException.class)
    public void getPublicKeyTestRequestError() {
        wireMockRule.stubFor(get("/realm/testPK")
                .willReturn(aResponse().withStatus(404)));
        KeycloakPKProvider ssoPublicKeyProvider = new KeycloakPKProvider("http://localhost:" + port + "/realm/testPK", "2");
        ssoPublicKeyProvider.getPublicKey();

    }

    private Certificate getCertificateFromKeystore(String path, String alias, String password) throws KeyStoreException {
        KeyStore keyStore = KeyStore.getInstance("jks");
        try (InputStream inputStream = Files.newInputStream(Paths.get(path))) {
            keyStore.load(inputStream, password.toCharArray());
            Key key = keyStore.getKey(alias, password.toCharArray());
            if (key instanceof PrivateKey) {
                return keyStore.getCertificate(alias);
            }
            return null;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}