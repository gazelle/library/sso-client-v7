package net.ihe.gazelle.ssov7.m2m.server.interlay.service;

import io.jsonwebtoken.Jwts;
import net.ihe.gazelle.ssov7.m2m.server.domain.GazelleJwtParser;
import net.ihe.gazelle.ssov7.m2m.server.utils.PublicKeyGenerator;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

import java.security.KeyPair;
import java.util.*;

import static org.junit.Assert.assertEquals;

public class GazelleJwtParserTest {
    private static final String ISSUER = "issuer";
    private static final String AUDIENCE = "http://localhost";
    private String jwt;
    private Date now;
    private Date expirationDate;
    GazelleJwtParser jwtParser;
    private String authorizedParty = "authorizedParty";
    private List<String> rolesList;
    private static final PublicKeyGenerator publicKeyGenerator = new PublicKeyGenerator();
    @Rule
    public final EnvironmentVariables environmentVariables = new EnvironmentVariables();

    @Before
    public void setUp() {
        KeyPair keyPair = initKeyPair();

        iniJwt(keyPair);
        jwtParser = new GazelleJwtParserImpl(jwt);
    }

    private void iniJwt(KeyPair keyPair) {
        authorizedParty = "authorizedParty";
        rolesList = Arrays.asList("role1", "role2");
        Calendar calendar = new GregorianCalendar();
        now = calendar.getTime();

        calendar.setTimeInMillis(calendar.getTimeInMillis() + 60000);
        expirationDate = calendar.getTime();

        jwt = Jwts.builder()
                .issuer(ISSUER)
                .issuedAt(now)
                .expiration(expirationDate)
                .audience().add(AUDIENCE).and()
                .claim("azp", authorizedParty)
                .claim("roles", rolesList)
                .claim("scope", "read write")
                .signWith(keyPair.getPrivate())
                .compact();
    }

    private KeyPair initKeyPair() {
        environmentVariables.set("JWT_VERIFY_PUBLIC_KEY_LOCATION", "expectedKey.pem");
        KeyPair keyPair = publicKeyGenerator.generateKeyPair();
        publicKeyGenerator.generatePublicKeyFileFromString("expectedKey.pem",
                publicKeyGenerator.getPublicKeyString(keyPair.getPublic()));
        return keyPair;
    }

    @Test
    public void getIssuedAt() {
        //test if the time is same in seconds
        assertEquals(now.getTime()/1000, jwtParser.getIssuedAt().getTime()/1000);
    }

    @Test
    public void getAudience() {
        Set<String> audience = new HashSet<>(Collections.singletonList(AUDIENCE));
        assertEquals(audience, jwtParser.getAudience());
    }

    @Test
    public void getIssuer() {
        assertEquals(ISSUER, jwtParser.getIssuer());
    }

    @Test
    public void getExpirationDate() {
        //test if the time is same in seconds
        assertEquals(expirationDate.getTime()/1000, jwtParser.getExpirationDate().getTime()/1000);
    }

    @Test
    public void getAuthorizationParty() {
        assertEquals(authorizedParty, jwtParser.getAuthorizationParty());
    }

    @Test
    public void getRoles() {
        assertEquals(rolesList, jwtParser.getRoles());
    }

    @Test
    public void getScope() {
        assertEquals(Arrays.asList("read", "write"), jwtParser.getScope());
    }
}