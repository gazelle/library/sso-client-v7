# Migration-guide

## Update to 5.X.X

In this major release, the user preferences are now in GUM and a client to retrieve them was made in SSO. To ensure that
everything works as intended please add the following in your project :

### Add Maven dependencies

In the of the application **war** add:

```xml
    <dependency>
         <groupId>net.ihe.gazelle</groupId>
         <artifactId>cas-client-ui-v7</artifactId>
         <version>${sso.client.v7.version}</version>
         <type>war</type>
     </dependency>
```

In the pom of the application **ear** add :

```xml
        <dependency>
            <groupId>net.ihe.gazelle</groupId>
            <artifactId>gum-client</artifactId>
            <version>${sso.client.v7.version}</version>
        </dependency>
```

In the pom of the application **ejb** add :
````xml
        <dependency>
            <groupId>net.ihe.gazelle</groupId>
            <artifactId>cas-client-v7</artifactId>
        </dependency>

````


In the xhtml file where the menu is, add the following:

```xhtml
    <!-- Fragment from cas-client-ui-v7    -->
    <ui:decorate template="/layout/_login_menu.xhtml" />

```

### Add user photo endpoint declaration

In pages.xml
```xml
    <page view-id="/userPhoto.xhtml" action="#{gumUserProfilePictureBean.getUserProfilePicture()}"/>
```

### Add new required environment variables

The environment variables for gum front:

```env
# GUM
GUM_FRONT_URL="https://my-gum-front"
GUM_REGISTRATION_URL="${GUM_FRONT_URL}/registration"
GUM_ACCOUNT_URL="${GUM_FRONT_URL}/account"
GUM_USERS_URL="${GUM_FRONT_URL}/users"
```

### Pull new Crowdin translations

New ui artifacts needs new translations. To pull the new translations, run the following command (pay attention to your .m2/settings.xml config file):

```bash
cd project_root
mvn crowdin:pull
```