package net.ihe.gazelle.ssov7.authn.interlay;

/**
 * Component used to determine how to authenticate a user.
 */
public interface Authenticator {

    /**
     * Authenticate the Identity
     * @return an AuthenticatorResource with attribute of a GazelleIdentity
     */
    AuthenticatorResource authenticate();

    /**
     * Is the Authenticator ready to log in
     * @return true if the user is ready to authenticate, false otherwise
     */
    boolean isReadyToLogin();

    /**
     * Return which type of logout is used for this Authenticator
     * @return a String of the logout type
     */
    String getLogoutType();

    /**
     * Logout the current user
     */
    void logout();
}
