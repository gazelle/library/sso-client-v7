/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2023 IHE
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.ssov7.authn.interlay.adapter;

import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.ssov7.authn.domain.GazelleLoginException;
import net.ihe.gazelle.ssov7.authn.interlay.Authenticator;
import net.ihe.gazelle.ssov7.authn.interlay.AuthenticatorProvider;
import net.ihe.gazelle.ssov7.authn.interlay.AuthenticatorResource;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Install;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Startup;
import org.jboss.seam.annotations.intercept.BypassInterceptors;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.security.Identity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.StringTokenizer;

@Name("org.jboss.seam.security.identity")
@Scope(ScopeType.SESSION)
@Install(precedence = Install.APPLICATION)
@BypassInterceptors
@Startup
public class GazelleIdentityImpl extends Identity implements GazelleIdentity {
    private static final long serialVersionUID = 6781264585892989766L;
    private static final String LOGGED_IN = "loggedIn";

    private static final Logger LOG = LoggerFactory.getLogger(GazelleIdentityImpl.class);

    private String organisationKeyword;
    private String firstName;
    private String lastName;
    private String email;
    private transient Authenticator activeAuthenticator;
    private transient AuthenticatorProvider authenticatorProvider = (AuthenticatorProvider) Component.getInstance("authenticatorProvider", true);

    public static GazelleIdentityImpl instance() {
        if (!Contexts.isSessionContextActive()) {
            throw new IllegalStateException("No active session context.");
        }
        GazelleIdentityImpl instance = (GazelleIdentityImpl) Component.getInstance(
                GazelleIdentityImpl.class,
                ScopeType.SESSION
        );
        if (instance == null) {
            throw new IllegalStateException("GazelleIdentityImpl could not be created.");
        }
        return instance;
    }

    @Override
    public boolean isLoggedIn() {
        refreshLoggedIn();
        return super.isLoggedIn();
    }

    @Override
    public String getUsername() {
        return super.getCredentials().getUsername();
    }

    @Override
    public String getFirstName() {
        return this.firstName;
    }

    @Override
    public String getLastName() {
        return this.lastName;
    }

    @Override
    public String getEmail() {
        return this.email;
    }

    @Override
    public String getOrganisationKeyword() {
        return this.organisationKeyword;
    }

    @Override
    public String getDisplayName() {
        if (StringUtils.isNotBlank(this.firstName) && StringUtils.isNotBlank(this.lastName)) {
            return this.firstName + " " + this.lastName;
        } else {
            return this.getUsername();
        }
    }

    @Override
    public boolean hasRole(String role) {
        refreshLoggedIn();
        return super.hasRole(role);
    }

    @Override
    public String login() {
        LOG.trace("login");
        try {
            if (!super.isLoggedIn()) {
                for (Authenticator authenticator : getAuthenticators()) {
                    AuthenticatorResource authenticatorResource = authenticator.authenticate();
                    if (authenticatorResource.isLoggedIn()) {
                        activeAuthenticator = authenticator;
                        return authenticateWithResource(authenticatorResource);
                    }
                }

                return super.login();
            }
            return LOGGED_IN;
        } catch (final RuntimeException e) {
            this.unAuthenticate();
            throw new GazelleLoginException(e);
        }
    }


    @Override
    public void logout() {
        LOG.trace("logout");
        activeAuthenticator.logout();
        super.logout();
    }

    @Override
    public void unAuthenticate() {
        super.unAuthenticate();
        organisationKeyword = null;
        firstName = null;
        lastName = null;
        email = null;
    }

    public String ssoLogout() {
        LOG.trace("ssoLogout");
        if (isLoggedIn()) {
            logout();
            String logoutType = activeAuthenticator.getLogoutType();
            activeAuthenticator = null;
            return logoutType;
        } else {
            return "noLogout";
        }
    }

    private void refreshLoggedIn() {
        if (!super.isLoggedIn() && isAnyAuthenticatorReady()) {
            login();
        }
    }

    private boolean isAnyAuthenticatorReady() {
        boolean isAnyReady = false;
        for (Authenticator authenticator : getAuthenticators()) {
            isAnyReady = isAnyReady || authenticator.isReadyToLogin();
        }
        return isAnyReady;
    }

    private void addRoles(String rolesArrayString) {
        if (rolesArrayString != null) {
            final StringTokenizer st = new StringTokenizer(rolesArrayString, "[,]");
            while (st.hasMoreElements()) {
                this.addRole(StringUtils.trimToNull((String) st.nextElement()));
            }
        }
    }

    private void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    private void setLastName(String lastName) {
        this.lastName = lastName;
    }

    private void setEmail(String email) {
        this.email = email;
    }

    private List<Authenticator> getAuthenticators() {
        return authenticatorProvider.getEnabledAuthenticators();
    }


    private String authenticateWithResource(AuthenticatorResource authenticatorResource) {
        if (authenticatorResource.isLoggedIn()) {
            preAuthenticate();
            acceptExternallyAuthenticatedPrincipal(authenticatorResource.getPrincipal());
            getCredentials().setUsername(authenticatorResource.getUsername());
            organisationKeyword = authenticatorResource.getOrganizationKeyword();
            addRoles(authenticatorResource.getRoles());
            setFirstName(authenticatorResource.getFirstName());
            setLastName(authenticatorResource.getLastName());
            setEmail(authenticatorResource.getEmail());
            postAuthenticate();
            return LOGGED_IN;
        }
        return null;
    }

    /**
     * Reload userService in case of deserialization.
     *
     * @param in InputStream of the UserValueFormatter.
     *
     * @throws  ClassNotFoundException if the class of a serialized object
     *          could not be found.
     * @throws  IOException if an I/O error occurs.
     * @see Serializable
     */
    private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        authenticatorProvider = (AuthenticatorProvider) Component.getInstance("authenticatorProvider", true);
    }
}
