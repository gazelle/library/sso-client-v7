package net.ihe.gazelle.ssov7.authn.interlay.dto;

/**
 * This class is only used to test KeycloakClientDTO as the coverage of the inheritors class does not add to the abstract
 * class KeycloakClientDTO.
 * @see KeycloakClientDTO
 */
public class TestKeycloakClientDTO extends KeycloakClientDTO {

    public static final String NO_PROTOCOL = "no-protocol";

    protected TestKeycloakClientDTO() {
        super(NO_PROTOCOL);
    }

}
